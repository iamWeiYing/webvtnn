package com.example.dto;

import com.example.entity.Product;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class ProductDto {

    private Product product;

    private List<String> linkImage = new ArrayList<>();
}
