package com.example.dto;

import lombok.Data;

@Data
public class ProductPayment {

    private Long idproduct;

    private Integer quantity;
}
