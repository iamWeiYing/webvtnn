package com.example.api;

import com.example.entity.Status;
import com.example.repository.StatusRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
@CrossOrigin
public class StatusApi {

    @Autowired
    private StatusRepository statusRepository;

    @GetMapping("/admin/all-status")
    public List<Status> doanhThu(){
        List<Status> list = statusRepository.findAll();
        return list;
    }
}
