package com.example.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.sql.Date;

@Entity
@Table(name = "users")
@Getter
@Setter
public class User{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    private String username;

    private String email;

    private String password;

    private String fullname;

    private String phone;

    private String address;

    private Boolean actived;

    private String activation_key;

    private Date createdDate;

    @ManyToOne
    @JoinColumn(name = "authority_name")
    private Authority authorities;
}

